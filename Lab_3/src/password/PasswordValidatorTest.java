package password;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


public class PasswordValidatorTest {
 


//	public PasswordValidatorTest() {
//		super();
//	
//	}

	@Test 
	public void testCheckPasswordLength()
	{
		//expecting checkpasswordlength to return a boolean value
		assertTrue("Invalid password length", 
				PasswordValidator.checkPasswordLength("abcdefgh") == true);
		
	}
	
	@Test
	public void testCheckTwoDigit()
	{
		assertTrue("Password must have at least two digits",
				PasswordValidator.checkTwoDigits("abcdef23") == true);
		
	}
}
 